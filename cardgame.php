<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Card Game</title>
    <link rel="shortcut icon" href="img/england.svg" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script type="text/javascript" src="bootstrap.min.css"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Link css -->
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <!-- Latest compiled JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="src/three.r95.min.js"></script>
    <script src="src/vanta.waves.min.js"></script>
    <!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
    <div class="header" id="myHeader">
        <nav class="blue navbar navbar-expand-sm">
            <div class="container nav english">
                <a href="index.php">
                <h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
                </a>
                <ul class="navbar-nav chu-mau-do right ">
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content">Hi <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></a>
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content" href="logout.php" class="btn ">Sign Out</a>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container padding-cardgame">
        <section class="memory-game">
            <div class="memory-card" data-framework="Tree">
                <img class="front-face" src="img/tree-3.svg" alt="Tree" />
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>
            <div class="memory-card" data-framework="Tree">
                <div class="front-face items-center">
                    <h3>
                        Tree</h3>
                </div>
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>

            <div class="memory-card" data-framework="party">
                <img class="front-face" src="img/party-17.svg" alt="Party" />
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>
            <div class="memory-card" data-framework="party">
                <div class="front-face items-center">
                    <h3>
                        Singin'</h3>
                </div>
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>
            <div class="memory-card" data-framework="construction">
                <img class="front-face" src="img/construction-36.svg" alt="Construction" />
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>
            <div class="memory-card" data-framework="construction">
                <div class="front-face items-center">
                    <h3>
                        construction</h3>
                </div>
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>
            <div class="memory-card" data-framework="iconstruction">
                <img class="front-face" src="img/iconstruction.svg" alt="Iconstruction" />
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>
            <div class="memory-card" data-framework="iconstruction">
                <div class="front-face items-center">
                    <h3>
                        citizen construction</h3>
                </div>
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>

            <div class="memory-card" data-framework="basketball">
                <img class="front-face" src="img/iconmonstr-basketball-2.svg" alt="Basketball" />
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>
            <div class="memory-card" data-framework="basketball">
                <div class="front-face items-center">
                    <h3>
                        basketball</h3>
                </div>
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>

            <div class="memory-card" data-framework="headphones">
                <img class="front-face" src="img/iconmonstr-headphones-16.svg" alt="Headphones" />
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>
            <div class="memory-card" data-framework="headphones">
                <div class="front-face items-center">
                    <h3>
                        headphones</h3>
                </div>
                <img class="back-face" src="img/logo.png" alt="JS Badge" />
            </div>
        </section>
    </div>
    <div class="dotted"></div>
    <div class="navbar navbar-expand-sm img-center2">
        <div class="container nav english">
            <ul class="navbar-nav chu-mau-do right">
                <a id="color-title" class="btn btn-primary right" href="chooseone.php">Back (Quay về)</a>
            </ul>
        </div>
    </div>
</body>
<script src="src/main.js"></script>

</html>