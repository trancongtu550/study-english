// VANTA.WAVES({
//   el: "#body",
//   mouseControls: true,
//   touchControls: true,
//   minHeight: 200.0,
//   minWidth: 200.0,
//   scale: 1.0,
//   scaleMobile: 1.0,
//   shininess: 5.0,
//   waveHeight: 24.0,
//   waveSpeed: 0.5,
//   zoom: 1.58,
// });

let popUpContents = [
    "Công thức với Động từ thường:<br>Khẳng định: S + V(s/es) + O<br>Phủ định: S + do/does not + V_inf + O<br>Nghi vấn: Do/Does + S + V_inf + O?<br>Công thức với Động từ tobe:<br>Khẳng định: S + am/is/are + O.<br>Phủ định: S + am/is/are not + O.<br>Nghi vấn: Am/is/are + S + O?",
    "Công thức:<br>Khẳng định: S + am/is/are + V_ing + …<br>Phủ định: S + am/is/are not + V_ing + …<br>Nghi vấn: Am/Is/Are + S + V_ing + …?",
    "Công thức:<br>Khẳng định: S + has/have + V3/ed + O<br>Phủ định: S + has/have not + V3/ed + O<br>Nghi vấn: Have/has + S + V3/ed + O?",
    "Công thức:<br>Câu khẳng định: S + has/have been + V_ing<br>Câu phủ định: S + has/have not been + V-ing<br>Câu nghi vấn: Have/Has + S + been + V-ing?",
    "Công thức với Động từ thường:<br>Câu khẳng định: S + V2/ed + O<br>Câu phủ định: S + did not + V_inf + O<br>Câu nghi vấn: Did + S + V_inf + O ?<br>Công thức với Động từ tobe:<br>Câu khẳng định: S + was/were + O<br>Câu phủ định: S + were/was not + O<br>Câu nghi vấn: Was/were + S + O?",
    "Công thức:<br>Câu khẳng định: S + were/ was + V_ing + O<br>Câu phủ định: S + were/was+ not + V_ing + O<br>Câu nghi vấn: Were/was+S+ V_ing + O?",
    "Công thức:<br>Câu khẳng định: S + had + V3/ed + O<br>Câu phủ định: S + had + not + V3/ed + O<br>Câu nghi vấn: Had + S + V3/ed + O?",
    "Công thức:<br>Câu khẳng định: S + had been + V_ing + O<br>Câu phủ định: S + had + not + been + V_ing + O<br>Câu nghi vấn: Had + S + been + V_ing + O?",
    "Công thức:<br>Câu khẳng định: S + shall/will + V(infinitive) + O<br>Câu phủ định: S + shall/will + not + V(infinitive) + O<br>Câu nghi vấn: Shall/will+S + V(infinitive) + O?",
    "Công thức:<br>Câu khẳng định: S + will/shall + be + V-ing<br>Câu phủ định: S + will/shall + not + be + V-ing<br>Câu nghi vấn: Will/shall + S + be + V-ing?",
    "Công thức:<br>Câu khẳng định: S + shall/will + have + V3/ed<br>Câu phủ định: S + shall/will not + have + V3/ed<br>Câu nghi vấn: Shall/Will+ S + have + V3/ed?",
    "Công thức:<br>Câu khẳng định: S + shall/will + have been + V-ing + O<br>Câu phủ định: S + shall/will not+ have + been + V-ing<br>Câu ghi vấn: Shall/Will + S+ have been + V-ing + O?",
];

function selectLession(id) {
    let popUpdocument = document.getElementById("popup-content");
    popUpdocument.innerHTML = popUpContents[id];
}


var emailArray = [];
var passwordArray = [];

var loginBox = document.getElementById("login");
var regBox = document.getElementById("register");
var forgetBox = document.getElementById("forgot");

var loginTab = document.getElementById("lt");
var regTab = document.getElementById("rt");

function regTabFun() {
    event.preventDefault();

    regBox.style.visibility = "visible";
    loginBox.style.visibility = "hidden";
    forgetBox.style.visibility = "hidden";

    regTab.style.backgroundColor = "rgb(12, 132, 189)";
    loginTab.style.backgroundColor = "rgba(11, 177, 224, 0.82)";
}

function loginTabFun() {
    event.preventDefault();

    regBox.style.visibility = "hidden";
    loginBox.style.visibility = "visible";
    forgetBox.style.visibility = "hidden";

    loginTab.style.backgroundColor = "rgb(12, 132, 189)";
    regTab.style.backgroundColor = "rgba(11, 177, 224, 0.82)";
}

function forTabFun() {
    event.preventDefault();

    regBox.style.visibility = "hidden";
    loginBox.style.visibility = "hidden";
    forgetBox.style.visibility = "visible";

    regTab.style.backgroundColor = "rgba(11, 177, 224, 0.82)";
    loginTab.style.backgroundColor = "rgba(11, 177, 224, 0.82)";

}


function register() {
    event.preventDefault();

    var email = document.getElementById("re").value;
    var password = document.getElementById("rp").value;
    var passwordRetype = document.getElementById("rrp").value;

    if (email == "") {
        alert("Email required.");
        return;
    } else if (password == "") {
        alert("Password required.");
        return;
    } else if (passwordRetype == "") {
        alert("Password required.");
        return;
    } else if (password != passwordRetype) {
        alert("Password don't match retype your Password.");
        return;
    } else if (emailArray.indexOf(email) == -1) {
        emailArray.push(email);
        passwordArray.push(password);

        alert(email + "  Thanks for registration. \nTry to login Now");

        document.getElementById("re").value = "";
        document.getElementById("rp").value = "";
        document.getElementById("rrp").value = "";
    } else {
        alert(email + " is already register.");
        return;
    }
}

function login() {
    event.preventDefault();

    var email = document.getElementById("se").value;
    var password = document.getElementById("sp").value;

    var i = emailArray.indexOf(email);

    if (emailArray.indexOf(email) == -1) {
        if (email == "") {
            alert("Email required.");
            return;
        }
        alert("Email does not exist.");
        return;
    } else if (passwordArray[i] != password) {
        if (password == "") {
            alert("Password required.");
            return;
        }
        alert("Password does not match.");
        return;
    } else {
        // alert(email + " yor are login Now \n welcome to our website.");

        document.getElementById("se").value = "";
        document.getElementById("sp").value = "";
        window.location = "index.php"
        return;
    }

}

function forgot() {
    event.preventDefault();

    var email = document.getElementById("fe").value;

    if (emailArray.indexOf(email) == -1) {
        if (email == "") {
            alert("Email required.");
            return;
        }
        alert("Email does not exist.");
        return;
    }

    alert("email is send to your email check it in 24hr. \n Thanks");
    document.getElementById("fe").value = "";
}




window.onscroll = function() { myFunction() };

var header = document.getElementById("myHeader");

var sticky = header.offsetTop;
header.classList.add("sticky");

function myFunction() {
    // if (window.pageYOffset > sticky) {
    //     header.classList.add("sticky");
    // } else {
    //     header.classList.remove("sticky");
    // }
}


//card game english
const cards = document.querySelectorAll('.memory-card');

let hasFlippedCard = false;
let lockBoard = false;
let firstCard, secondCard;

function flipCard() {
  if (lockBoard) return;
  if (this === firstCard) return;

  this.classList.add('flip');

  if (!hasFlippedCard) {
    hasFlippedCard = true;
    firstCard = this;

    return;
  }

  secondCard = this;
  checkForMatch();
}

function checkForMatch() {
  let isMatch = firstCard.dataset.framework === secondCard.dataset.framework;

  isMatch ? disableCards() : unflipCards();
}

function disableCards() {
  firstCard.removeEventListener('click', flipCard);
  secondCard.removeEventListener('click', flipCard);

  resetBoard();
}

function unflipCards() {
  lockBoard = true;

  setTimeout(() => {
    firstCard.classList.remove('flip');
    secondCard.classList.remove('flip');

    resetBoard();
  }, 1500);
}

function resetBoard() {
  [hasFlippedCard, lockBoard] = [false, false];
  [firstCard, secondCard] = [null, null];
}

(function shuffle() {
  cards.forEach(card => {
    let randomPos = Math.floor(Math.random() * 12);
    card.style.order = randomPos;
  });
})();

cards.forEach(card => card.addEventListener('click', flipCard));