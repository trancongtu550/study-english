<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Tense</title>
	<link rel="shortcut icon" href="img/england.svg" />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="bootstrap.min.css"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Link css -->
	<link rel="stylesheet" type="text/css" href="css/theme.css">
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="src/three.r95.min.js"></script>
	<script src="src/vanta.waves.min.js"></script>
	<!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
	<div class="header" id="myHeader">
		<nav class="blue navbar navbar-expand-sm">
			<div class="container nav english">
				<a href="index.php">
				<h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
				</a>
				<ul class="navbar-nav chu-mau-do right ">
					<a id="color-title" class="nav-item nav-link chu-mau-do head-content">Hi <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></a>
					<a id="color-title" class="nav-item nav-link chu-mau-do head-content" href="logout.php" class="btn ">Sign Out</a>
				</ul>
			</div>
		</nav>
	</div>
	<div class="modal fade" id="popUpModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Công thức</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p id="popup-content">Empty</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="popUpModel-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Login</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<a id="color-title" class="nav-item nav-link chu-mau-do head-content">Hi <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></a>
				</div>
			</div>
		</div>
	</div>
	<div>
		<section class="jumbotron text-center">
			<div class="container">
				<h1>Tense Lesson (Bài học)<p class="lead text-muted">Basic English (Tiếng Anh cơ bản)<br>
				</h1>

			</div>
		</section>
	</div>
	<div class="row padding-border-tense">
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(0)">
				<div class="row ">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/classic-blue.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì hiện tại đơn - Present Simple</h1>
						<p class="card-text">Diễn tả một sự thật hiển nhiên, một chân lý,
							một thói quen, sở thích hay hành động được lặp đi lặp lại ở hiện tại
							một lịch trình, chương trình, một thời gian biểu.
						</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(1)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/present.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì hiện tại tiếp diễn – Present continuous tense</h1>
						<p class="card-text">Diễn tả hành động đang xảy ra và kéo dài ở hiện tại,
							dự định, kế hoạch sắp xảy ra trong tương lai đã định trước.
						</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(2)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/growth.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì hiện tại hoàn thành – Present perfect tense</h1>
						<p class="card-text">Diễn tả 1 hành động xảy ra trong quá khứ nhưng vẫn còn ở hiện tại và tương lai, hành động xảy ra và kết quả trong quá khứ nhưng không nói rõ thời gian xảy ra</p>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="row padding-border-tense">
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(3)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/continuous.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì hiện tại hoàn thành tiếp diễn – Present perfect continuous tense</h1>
						Diễn tả hành động xảy ra diễn ra liên tục trong quá khứ, tiếp tục kéo dài đến hiện tại, hành động vừa kết thúc, mục đích nêu kết quả của hành động.</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(4)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/past.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì quá khứ đơn – Past simple tense</h1>
						<p class="card-text">Diễn tả một hành động đã xảy ra và đã kết thúc tại thời điểm trong quá khứ,
							những hành động xảy ra liên tiếp tại thời trong điểm quá khứ.

						</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(5)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/historys.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì quá khứ tiếp diễn – Past continuous tense</h1>
						<p class="card-text">Để diễn tả hành động đang xảy ra tại một thời điểm trong quá khứ,
							một hành động đang xảy ra trong quá khứ thì có một hành động khác xen vào,
							những hành động xảy ra song song với nhau.
						</p>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="row padding-border-tense">
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(6)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/list.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì quá khứ hoàn thành – Past perfect tense</h1>
						<p class="card-text">Diễn tả hành động đã hoàn thành trước một thời điểm trong quá khứ
							Diễn đạt một hành động đã xảy ra trước một hành động khác trong quá khứ. Hành động xảy ra trước dùng quá khứ hoàn thành – xảy ra sau dùng quá khứ đơn</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(7)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/paste.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì quá khứ hoàn thành tiếp diễn – Past perfect continuous tense</h1>
						<p class="card-text">Diễn tả một hành động xảy ra liên tục trước một hành động khác trong quá khứ,
							một hành động xảy ra kéo dài liên tục trước một thời điểm được xác định trong quá khứ. </p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(8)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/modern.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì tương lai đơn – Simple future tense</h1>
						<p class="card-text">Diễn tả một dự đoán không có căn cứ xác định,
							dự định đột xuất xảy ra ngay lúc nói,
							lời ngỏ ý, hứa hẹn, đề nghị, đe dọa.
						</p>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="row padding-border-tense">
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(9)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/driverless-car.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì tương lai tiếp diễn – Future continuous tense</h1>
						<p class="card-text">Diễn tả về một hành động xảy ra trong tương lai tại thời điểm xác định,
							về một hành động đang xảy ra trong tương lai thì có hành động khác chen vào.
						</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(10)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/chatbot.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì tương lai hoàn thành – Future perfect tense</h1>
						<p class="card-text">Diễn tả về một hành động hoàn thành trước một thời điểm xác định trong tương lai,
							về một hành động hoàn thành trước một hành động khác trong tương lai. </p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4">
			<a class="border-tense" role="button" tabindex="0" data-toggle="modal" data-target="#popUpModel" onclick="selectLession(11)">
				<div class="row">
					<div class="col-4 img-center-border"><img class="img-center2" src="img/360-image.svg" alt="Card image" style="width:100%; border-radius: 16px; "></div>
					<div class="col-8">
						<h1 class="border-part3">Thì tương lai hoàn thành tiếp diễn – Future perfect continuous tense</h1>
						<p class="card-text">Diễn tả một hành động xảy ra trong quá khứ tiếp diễn liên tục đến một thời điểm cho trước trong tương lai.</p>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="dotted"></div>
	<div class="navbar navbar-expand-sm img-center2">
		<div class="container nav english">
			<ul class="navbar-nav chu-mau-do right">
				<a id="color-title" class="btn btn-primary right" href="chooseone.php">Back (Quay về)</a>
			</ul>
		</div>
	</div>
</body>
<script src="src/main.js"></script>

</html>