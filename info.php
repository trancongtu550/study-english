<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Info</title>
    <link rel="shortcut icon" href="img/england.svg" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script type="text/javascript" src="bootstrap.min.css"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Link css -->
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <!-- Latest compiled JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="src/three.r95.min.js"></script>
    <script src="src/vanta.waves.min.js"></script>
    <!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
    <div class="header" id="myHeader">
        <nav class="blue navbar navbar-expand-sm">
            <div class=" container nav english">
                <a href="index.php">
                <h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
                </a>
                <ul class="navbar-nav chu-mau-do right ">
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content" href="chooseone.php" class="btn ">Back</a>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <main>
            <div>
                <section class="margin-section">
                    <div class=" aboutus">
                        <h1>
                            Giới thiệu
                        </h1>
                    </div>
                    <div class="row center-row">
                        <div class="col-4 center font-center-about-us">
                            <a class="center-about-us" href="info.php">Về đồ án</a>
                        </div>
                        <div class="col-4 center font-center-about-us">
                            <a class="center-about-us" href="approach.php">Phương pháp</a>

                        </div>
                        <div class="col-4 center font-center-about-us">
                            <a class="center-about-us" href="group.php">Nhóm</a>
                        </div>
                    </div>
                    <div class="dotted"></div>
                    <section class="margin-section">
                        <div class="">
                            <div class="row">
                                <div class="col-sm-5 img-center">
                                    <img class="" src="img/aboutus.png" alt="Card image" style="width:70%">
                                </div>
                                <div class="col-sm-7">
                                    <h1 class="word-margin">
                                        Người sáng lập
                                    </h1>
                                    <span>
                                        Trần Công Tú <br>
                                        Nguyễn Phúc Nguyên <br>
                                        Trường Hoàng Long
                                    </span>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="background-info">
                        <img class="" src="img/Capture.png" alt="Card image" style="width:100%">
                    </section>
                </section>
            </div>
        </main>
    </div>
    <div class="dotted"></div>
    <div class="footer backgroud">
        <div class="header-footer "></div>
    </div>
</body>
<script src="src/main.js"></script>

</html>