<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Web</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Link css -->
	<link rel="stylesheet" type="text/css" href="../css/theme.css">
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<!-- <script src="src/three.r95.min.js"></script> -->
	<!-- <script src="src/vanta.waves.min.js"></script> -->
	<!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
	<nav id="myHeader" class="blue navbar navbar-expand-sm">
		<div class="container nav english">
			<a href="index.php">
				<h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> English<span class="badge badge-primary new">New</span></h1>
			</a>
		</div>
	</nav>
	<div class="container">
		<section class="margin-section">
			<div class="center-section">
				<div class="img-center">
					<a href="Videolesson/test.php"><iframe title="vimeo-player" src="https://player.vimeo.com/video/392862100" width="640" height="480" frameborder="0" allowfullscreen></iframe></a>
				</div>
			</div>
		</section>
		<div class="dotted"></div>
		<div class="tabcontents">
			<div id="view" style="display: block;">
				<div id="dropdown">
					<div class="hide" id="tutorialcode">
						<div class="transcript" id="transcript">
							<p>The French can’t speak English; or is it the Americans can’t speak French? Actually, neither is really true. Since well before the American War of Independence, the French and the Americans have had difficulty with each other’s language, sometimes comically.</p>
							<p>It is true that few Americans achieve fluency in French. Even Barack Obama has joked about American’s unwillingness to learn second languages. He pointed out that many Europeans speak a variety of languages but when Americans go abroad, all they can say is, “Merci beaucoup.” On the other hand, English is not so often heard in Paris as in other European cities, and many are reluctant to speak it. Some would argue that French rolls so beautifully off the tongue, why bother with English? There is however, no denying the importance of English in the era of globalization.</p>
							<p>So, what’s the problem? Well, if you bother to ask second language learners from both countries, you’ll find they have two things in common. First, each is afraid of making embarrassing mistakes! Second, most Americans expect everyone to speak English, and the French naturally find that a bit impolite. The solution? A little self confidence and cultural tolerance go a long way. Umm…, Parlez vous Anglaise?” </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="dotted"></div>
	<div class="navbar navbar-expand-sm img-center2">
		<div class="container nav english">
			<ul class="navbar-nav chu-mau-do right">
				<a id="color-title" class="btn btn-primary right" href="../chooseone.php">Back (Quay về)</a>
			</ul>
		</div>
	</div>
</body>
<script src="/src/main.js"></script>

</html>