<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Web</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Link css -->
	<link rel="stylesheet" type="text/css" href="../css/theme.css">
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<!-- <script src="src/three.r95.min.js"></script> -->
	<!-- <script src="src/vanta.waves.min.js"></script> -->
	<!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
<nav id="myHeader" class="blue navbar navbar-expand-sm">
		<div class="container nav english">
			<a href="index.php">
				<h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> English<span class="badge badge-primary new">New</span></h1>
			</a>
		</div>
	</nav>
	<div class="container">
		<section class="margin-section">
			<div class="center-section">
				<div class="img-center">
					<a href="Videolesson/test.php"><iframe title="vimeo-player" src="https://player.vimeo.com/video/392862410" width="640" height="480" frameborder="0" allowfullscreen></iframe></a>
				</div>
			</div>
		</section>
		<div class="dotted"></div>
		<div class="tabcontents">
			<div id="view" style="display: block;">
				<div id="dropdown">
					<div class="hide" id="tutorialcode">
						<div class="transcript" id="transcript">
							<p>The house is rocking, the crowd is going wild and for a brief moment you are a guitar hero; at least in the virtual world, but when did it all begin?</p>
							<p>While acoustic guitars have been around for centuries, the exact date of the first attachment of a pickup to the body of a guitar is unknown. Pickups feel the vibration of the strings and send it through an amplifier to produce sound. They were developed in the early 1920’s, but were more like small microphones than modern pickups. </p>
							<p>In 1931, Adolph Rickenbacher produced a Hawaiian guitar that came to be known as the “Frying Pan.” It was the first guitar to use a modern style pickup. Electric hollow-bodied jazz guitars became more popular during the Big Band era of the 30’s and 40’s when amplifiers were needed to compete with loud brass instruments such as the trumpet and the trombone. </p>
							<p>The style most common today is the solid body electric guitar. Musician and inventor Les Paul who had also experimented with pickups, created the first solid body guitar in 1941. It was made of solid wood with no sound holes.</p>
							<p>In 1946, another inventor, Leo Fender designed the first commercially successful solid-body electric guitar. Today the Gibson “Les Paul” and the Fender “Stratocaster” remain the two most popular guitars in the world. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="dotted"></div>
	<div class="navbar navbar-expand-sm">
		<div class="container nav english">
			<ul class="navbar-nav chu-mau-do right">
			<a id="color-title" class="btn btn-primary right" href="../videolesson.php">Back (Quay về)</a>
			</ul>
			<ul class="navbar-nav chu-mau-do right">
				<a id="color-title" class="btn btn-primary right" href="lesson12.php" role="button">Next lesson (Tiếp theo)</a>
			</ul>
		</div>
	</div>
</body>
<script src="/src/main.js"></script>

</html>