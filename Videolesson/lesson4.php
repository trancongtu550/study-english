<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Web</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Link css -->
	<link rel="stylesheet" type="text/css" href="../css/theme.css">
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<!-- <script src="src/three.r95.min.js"></script> -->
	<!-- <script src="src/vanta.waves.min.js"></script> -->
	<!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
	<nav id="myHeader" class="blue navbar navbar-expand-sm">
		<div class="container nav english">
			<a href="index.php">
				<h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> English<span class="badge badge-primary new">New</span></h1>
			</a>
		</div>
	</nav>
	<div class="container">
		<section class="margin-section">
			<div class="center-section">
				<div class="img-center">
					<a href="Videolesson/test.php"><iframe title="vimeo-player" src="https://player.vimeo.com/video/392868593" width="640" height="480" frameborder="0" allowfullscreen></iframe></a>
				</div>
			</div>
		</section>
		<div class="dotted"></div>
		<div class="tabcontents">
			<div id="view" style="display: block;">
				<div id="dropdown">
					<div class="hide" id="tutorialcode">
						<div class="transcript" id="transcript">
							<p>Novels written and read on mobile telephones have been all the rage in Japan for six years now. Most of these stories deal with romance and are especially trendy among high school girls. But people who harbor the stereotype that love stories belong to the feminine realm might be surprised to hear that the first mobile romance, Deep Love, was written by a man who goes by the pen name “Yoshi”. In 2001, Yoshi distributed leaflets advertising his debut in Shibuya, Tokyo’s entertainment district, and a trend was born. </p>
							<p>The typical mobile novel is 200 to 500 pages long and can be downloaded for about $10. Each website-like page contains about 500 Japanese characters. Recently, mobile novelists have been trying to reach a wider audience by venturing into other genres such as horror and science fiction. But perhaps the feature that appeals the most to young fans is the interactive nature of the mobile novels. Readers can send feedback and participate in shaping the story. Even teachers and parents are enthusiastic about this new trend that encourages kids to read. </p>
							<p>There is considerable disagreement amongst officials in the publishing industry whether these novels are here to stay or just a passing fad. Some critics of the novels say they reach a small minority of readers who likely do not read much anyway. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="dotted"></div>
	<div class="navbar navbar-expand-sm">
		<div class="container nav english">
			<ul class="navbar-nav chu-mau-do right">
			<a id="color-title" class="btn btn-primary right" href="../videolesson.php">Back (Quay về)</a>
			</ul>
			<ul class="navbar-nav chu-mau-do right">
				<a id="color-title" class="btn btn-primary right" href="lesson5.php" role="button">Next lesson (Tiếp theo)</a>
			</ul>
		</div>
	</div>
</body>
<script src="/src/main.js"></script>

</html>