<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Web</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Link css -->
	<link rel="stylesheet" type="text/css" href="../css/theme.css">
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<!-- <script src="src/three.r95.min.js"></script> -->
	<!-- <script src="src/vanta.waves.min.js"></script> -->
	<!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
	<nav id="myHeader" class="blue navbar navbar-expand-sm">
		<div class="container nav english">
			<a href="index.php">
				<h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> English<span class="badge badge-primary new">New</span></h1>
			</a>
		</div>
	</nav>
	<div class="container">
		<section class="margin-section">
			<div class="center-section">
				<div class="img-center">
					<a href="Videolesson/test.php"><iframe title="vimeo-player" src="https://player.vimeo.com/video/392862102" width="640" height="480" frameborder="0" allowfullscreen></iframe></a>
				</div>
			</div>
		</section>
		<div class="dotted"></div>
		<div class="tabcontents">
			<div id="view" style="display: block;">
				<div id="dropdown">
					<div class="hide" id="tutorialcode">
						<div class="transcript" id="transcript">
							<p>On the subject of American baseball, noted British comedian John Cleese wrote in his Letter to America: ”You should stop playing baseball. It is not reasonable to host an event called the 'World Series' for a game which is not played outside of America." Fans of the sport cried foul! After all, it is common knowledge that the World Series is so named not because the winners are considered the world's champions, but because the competition was originally sponsored by the New York World newspaper; or was it?</p>
							<p>The modern World Series began in 1903, when the National League's Pittsburgh Pirates agreed to a playoff series against the Boston Americans (soon the Boston Red Sox) of the American League. The first contests between the two league champions were reported under various titles — "world’s championship series," "championship series," "world's series," and finally, "World Series."</p>
							<p>The New York World newspaper was established in 1860, and acquired by Joseph Pulitzer in 1883. It eventually became renowned for its sensationalist tabloid or "yellow” journalism. In the months leading up to the 1903 World's Championship Series there was not a word to suggest any link between the paper and the series. </p>
							<p>A spokesman for The National Baseball Hall of Fame has added, "There's no evidence suggesting it was ever sponsored by the New York World newspaper.” Perhaps Mr. Cleese had a point after all. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="dotted"></div>
	<div class="navbar navbar-expand-sm">
		<div class="container nav english">
			<ul class="navbar-nav chu-mau-do right">
			<a id="color-title" class="btn btn-primary right" href="../videolesson.php">Back (Quay về)</a>
			</ul>
			<ul class="navbar-nav chu-mau-do right">
				<a id="color-title" class="btn btn-primary right" href="lesson9.php" role="button">Next lesson (Tiếp theo)</a>
			</ul>
		</div>
	</div>
</body>
<script src="/src/main.js"></script>

</html>