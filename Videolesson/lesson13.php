<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Web</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Link css -->
	<link rel="stylesheet" type="text/css" href="../css/theme.css">
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<!-- <script src="src/three.r95.min.js"></script> -->
	<!-- <script src="src/vanta.waves.min.js"></script> -->
	<!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
<nav id="myHeader" class="blue navbar navbar-expand-sm">
		<div class="container nav english">
			<a href="index.php">
				<h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> English<span class="badge badge-primary new">New</span></h1>
			</a>
		</div>
	</nav>
	<div class="container">
		<section class="margin-section">
			<div class="center-section">
				<div class="img-center">
					<a href="Videolesson/test.php"><iframe title="vimeo-player" src="https://player.vimeo.com/video/392862426" width="640" height="480" frameborder="0" allowfullscreen></iframe></a>
				</div>
			</div>
		</section>
		<div class="dotted"></div>
		<div class="tabcontents">
			<div id="view" style="display: block;">
				<div id="dropdown">
					<div class="hide" id="tutorialcode">
						<div class="transcript" id="transcript">
							<p>MTV unquestionably revolutionized the music industry. The network debuted in 1981 to a few thousand households on a single cable system. It finished the year with nearly 21 million subscribers.</p>
							<p>In addition to its music programming, MTV has a long history of promoting social, political, and environmental activism. In doing so, it has come under fire from both liberal and conservative media watchdogs, as well as religious groups over such issues as censorship, political correctness, and perceived negative moral influence on young people</p>
							<p>MTV became the first to run TV spots promoting safe sex and AIDS awareness; issues considered far too controversial for major broadcast networks. It also addressed the problem of substance abuse with a series of “Just Say No” anti- drug spots and a “Rock Against Drugs” campaign.</p>
							<p>MTV entered the political arena when it launched the “Choose or Lose” campaign to get students politically involved by increasing voter registration. ”Fight for your Rights” championed the causes of anti violence and anti-discrimination. The Bill and Melinda Gates Foundation and MTV teamed up to form “think: Education” encouraging Students to graduate from high school and be prepared for the responsibilities of college and work. The channel also aired a series of pro-conservation ads called “Break the Addiction” advocating reduced energy consumption.</p>
							<p>MTV continues to address current political issues. “think MTV”, the network’s vehicle for social activism, invites young people to” choose the issues that resonate most and take action to make a positive change.” </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="dotted"></div>
	<div class="navbar navbar-expand-sm">
		<div class="container nav english">
			<ul class="navbar-nav chu-mau-do right">
			<a id="color-title" class="btn btn-primary right" href="../videolesson.php">Back (Quay về)</a>
			</ul>
			<ul class="navbar-nav chu-mau-do right">
				<a id="color-title" class="btn btn-primary right" href="lesson14.php" role="button">Next lesson (Tiếp theo)</a>
			</ul>
		</div>
	</div>
</body>
<script src="/src/main.js"></script>

</html>