<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Web</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Link css -->
	<link rel="stylesheet" type="text/css" href="../css/theme.css">
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<!-- <script src="src/three.r95.min.js"></script> -->
	<!-- <script src="src/vanta.waves.min.js"></script> -->
	<!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
<nav id="myHeader" class="blue navbar navbar-expand-sm">
		<div class="container nav english">
			<a href="index.php">
				<h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> English<span class="badge badge-primary new">New</span></h1>
			</a>
		</div>
	</nav>
	<div class="container">
		<section class="margin-section">
			<div class="center-section">
				<div class="img-center">
					<a href="Videolesson/test.php"> <iframe title="vimeo-player" src="https://player.vimeo.com/video/392868579" width="640" height="480" frameborder="0" allowfullscreen></iframe></a>
				</div>
			</div>
		</section>
		<div class="dotted"></div>
		<div class="tabcontents">
			<div id="view" style="display: block;">
				<div id="dropdown">
					<div class="hide" id="tutorialcode">
						<div class="transcript" id="transcript">
							<p>
								<p>The Amazon Rainforest is a large tropical rainforest found in South America in the Amazon Basin. The forest covers over five million square kilometers, covering land in nine countries. The forest can be found in Brazil, Colombia, Peru, Venezuela, Ecuador, Bolivia, Guyana, Suriname and French Guiana. Most of the forest, about 60% of its mass, exists in Brazil.</p>
								<p>The rainforest contains an enormous diversity of plant life, with some experts estimating that in just one square kilometer of rainforest, biologists can examine over 75,000 types of trees and 150,000 examples of higher plants. One square kilometer can also hold over 90,000 tons of plant life, which is the largest collection of living plants on earth.</p>
								<p>Naturally, with all its abundance of plant life, the Amazon Rainforest also holds the largest collection of animal species on earth. There are over 3,000 species of recognized fish that reside in the Amazon River. The number of recognized fish is still growing and some experts put the figures at 5,000.</p>
								<p>Inland, the rainforest is host to thousands of other species as well. The region is home to over two million insects and thousands of birds and mammals. At last count, the list of recognized species contained 1,294 birds, 427 mammals, 428 amphibians and 378 reptiles. It is estimated that about one-third of all species on earth reside in the Amazon. Making it, along with its plant life, the richest source of bio-diversity in the world.</p>ulture.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="dotted"></div>
	<div class="navbar navbar-expand-sm">
		<div class="container nav english">
			<ul class="navbar-nav chu-mau-do right">
			<a id="color-title" class="btn btn-primary right" href="../videolesson.php">Back (Quay về)</a>
			</ul>
			<ul class="navbar-nav chu-mau-do right">
				<a id="color-title" class="btn btn-primary right" href="lesson3.php" role="button">Next lesson (Tiếp theo)</a>
			</ul>
		</div>
	</div>
</body>
<script src="/src/main.js"></script>

</html>