<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>English Basic</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="shortcut icon" href="img/england.svg" />

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="bootstrap.min.css"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Link css -->
	<link rel="stylesheet" type="text/css" href="css/theme.css">
	<!-- Latest compiled JavaScript -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
	<script src="src/three.r95.min.js"></script>
	<script src="src/vanta.waves.min.js"></script>
	<!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
	<div class="header" id="myHeader">
		<nav class="blue navbar navbar-expand-sm">
			<div class=" container nav english">
				<a href="index.php">
				<h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
				</a>
			</div>
		</nav>
	</div>
	<div class="jumbotron padding-center">
		<div class="row ">
			<div class="col-sm-6 img-center">
				<img class="img-center" src="img/eng2.jpg" alt="Card image" style="width:80%">
			</div>
			<div class="col-sm-6 img-center">
				<h1 class="display-4">Welcome !</h1>
				<p class="lead">Đây là trang web giúp bạn cải thiện trình độ tiếng anh của bản thận, về mặt giao tiếp và ứng xử.</p>
				<hr class="my-4">
				<p>Hãy dùng thử dịch vụ của chúng tôi, để biết thêm chi tiết.</p>
				<a class="btn btn-primary btn-lg" href="register.php" role="button">Bắt đầu</a>
			</div>
		</div>
	</div>
	<div class="container">
		<section class="margin-section">
			<div class="center-section">
				<div class="row">
					<div class="col-sm-4">
						<img class="img-center" src="img/why.jpg" alt="Card image" style="width:80%">
					</div>
					<div class="col-sm-8">
						<h1>
							Tại sao chúng ta nên học Tiếng Anh ?
						</h1>
						<span>
							1. Tiếng Anh là ngôn ngữ được sử dụng rộng rãi nhất. <br>
							2. Tiếng Anh mở ra nhiều cơ hội. <br>
							3. Tiếng Anh giúp bạn hấp dẫn hơn trong mắt nhà tuyển dụng. <br>
							4. Nhờ tiếng Anh, bạn có thể tiếp cận các trường đại học hàng đầu. <br>
							5. Tiếng Anh cho bạn kiến thức phong phú.
						</span>
					</div>
				</div>
			</div>
		</section>
		<div class="dotted"></div>
		<section class="margin-section">
			<div class="center-section">
				<div class="row">
					<div class="col-sm-8">
						<h1>
							Giao tiếp với người các nước khác nhau.
						</h1>
						<span>
							Các cách giao tiếp ứng xử, có thể học được dễ dàng nếu như bạn biết được Tiếng Anh, bạn có thể đi đó đây mà không phải ngại mình là người nước khác.<br>
							Tự tin giao tiếp với người bản xứ để học thêm nhiều điều mới lạ.<br>
							Thay đổi lối sống hằng ngày, có thể nói chuyện với AI, đặt lịch, hay những thứ được setup bằng Tiếng Anh.

						</span>
					</div>
					<div class="col-sm-4">
						<img class="img-center" src="img/hand.jpg" alt="Card image" style="width:80%">
					</div>
				</div>
			</div>
		</section>
		<div class="dotted"></div>
		<section class="margin-section">
			<div class="center-section">
				<div class="row">
					<div class="col-sm-4">
						<img class="img-center" src="img/brain.gif" alt="Card image" style="width:80%">
					</div>
					<div class="col-sm-8">
						<h1>
							Phương pháp học Tiếng Anh
						</h1>
						<span>
							1. Vượt qua tâm lý sợ hãi và kiên trì đến cùng. <br>
							2. Chuẩn bị vốn ngôn ngữ “từ vựng” vững chắc. <br>
							3. Dừng việc học ngữ pháp theo cách truyền thống. <br>
							4. Phát âm chuẩn và lưu loát là 2 câu chuyện hoàn toàn khác nhau. <br>
							5. Tự tạo môi trường giao tiếp tiếng Anh và đắm chìm trong đó.
						</span>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="dotted"></div>
	<div class="footer backgroud">
		<div class="header-footer">
			<div>
				<div>
					<h1>
						Học Tiếng Anh cùng chúng tôi
					</h1>
				</div>
			</div>
			<div class="content-footer">
				<div class="on-content-footer">
					<div class="padding-footer">
						<div class="font-footer">Giới thiệu</div>
						<ul>
							<li><a class="font-white" href="english.php">Khoá học</a><br></li>
							
							<li><a class="font-white" href="approach.php">Phương pháp</a><br></li>
						</ul>
					</div>
					<div class="padding-footer">
						<div class="font-footer">Sản phẩm</div>
						<ul>
							<li><a class="font-white" href="book.php">Sách</a><br></li>
							<li><a class="font-white" href="history.php">Lịch sử</a><br></li>
						</ul>
					</div>
					<div class="padding-footer">
						<div class="font-footer">Hỗ trợ</div>
						<ul>
							<li><a class="font-white" href="info.php">Về phía website</a><br></li>
						</ul>
					</div>
					<div class="padding-footer">
						<div class="font-footer">Mạng xã hội</div>
						<ul>
							<li><a class="font-white">Facebook</a><br></li>
							<li><a class="font-white" href="https://www.instagram.com/meownbobo/">Instagram</a><br></li>
							<li><a class="font-white" href="https://www.youtube.com/channel/UCV1ygnPRpMpSw8Xt4D7L0Sw/">Youtube</a><br></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="src/main.js"></script>

</html>