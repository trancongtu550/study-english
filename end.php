<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Last page</title>
	<link rel="shortcut icon" href="img/england.svg" />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="bootstrap.min.css"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Link css -->
	<link rel="stylesheet" type="text/css" href="css/theme.css">
	<!-- Latest compiled JavaScript -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
	<script src="src/three.r95.min.js"></script>
	<script src="src/vanta.waves.min.js"></script>
	<!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
	<div class="header" id="myHeader">
		<nav class="blue navbar navbar-expand-sm">
			<div class=" container nav english">
				<a href="index.php">
				<h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
				</a>
			</div>
		</nav>
	</div>
	<div class="jumbotron text-center">
		<h1 class="display-3">Thank You!</h1>
		<p class="lead"><strong>Cám ơn vì đã sử dụng dịch vụ của chúng tôi</strong>.</p>
		<hr>
		<p>
			Having trouble? <a href="info.php">Contact us</a>
		</p>
		<p class="lead">
			<a class="btn btn-primary btn-sm" href="welcome.php" role="button">Continue to homepage</a>
		</p>
	</div>
	<div>

	</div>
	<div class="dotted"></div>
	<div class="footer backgroud">
		<div class="header-footer">
			<div>
				<div>
					<h1>
						Học Tiếng Anh cùng chúng tôi
					</h1>
				</div>
			</div>
			<div class="content-footer">
				<div class="on-content-footer">
					<div class="padding-footer">
						<div class="font-footer">Giới thiệu</div>
						<ul>
							<li><a class="font-white" href="english.php">Khoá học</a><br></li>

							<li><a class="font-white" href="approach.php">Phương pháp</a><br></li>
						</ul>
					</div>
					<div class="padding-footer">
						<div class="font-footer">Sản phẩm</div>
						<ul>
							<li><a class="font-white" href="book.php">Sách</a><br></li>
							<li><a class="font-white" href="history.php">Lịch sử</a><br></li>
						</ul>
					</div>
					<div class="padding-footer">
						<div class="font-footer">Hỗ trợ</div>
						<ul>
							<li><a class="font-white" href="careers.php">Liên hệ với admin</a><br></li>
							<li><a class="font-white">Thảo Luận</a><br></li>
							<li><a class="font-white" href="info.php">Về phía website</a><br></li>
						</ul>
					</div>
					<div class="padding-footer">
						<div class="font-footer">Mạng xã hội</div>
						<ul>
							<li><a class="font-white">Facebook</a><br></li>
							<li><a class="font-white" href="https://www.instagram.com/meownbobo/">Instagram</a><br></li>
							<li><a class="font-white" href="https://www.youtube.com/channel/UCV1ygnPRpMpSw8Xt4D7L0Sw/">Youtube</a><br></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="src/main.js"></script>

</html>