<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>History</title>
    <link rel="shortcut icon" href="img/england.svg" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script type="text/javascript" src="bootstrap.min.css"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Link css -->
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <!-- Latest compiled JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="src/three.r95.min.js"></script>
    <script src="src/vanta.waves.min.js"></script>
    <!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
    <div class="header" id="myHeader">
        <nav class="blue navbar navbar-expand-sm">
            <div class="container nav english">
                <a href="index.php">
                <h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
                </a>
                <ul class="navbar-nav chu-mau-do right ">
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content">Hi <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></a>
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content" href="logout.php" class="btn ">Sign Out</a>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <main>
            <section class="margin-section">
                <div class="history-br">
                    <h1 class="padding-history">
                        Lịch sử của Tiếng Anh
                    </h1>
                    <ol>
                        <div class="dotted"></div>
                        <div class="row">
                            <div class="col-5">
                                <img class=" img-center" src="img/student.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                            </div>
                            <div class="col-7">
                                <li>
                                    <h4 class="color-black">Sơ lược về quá trình hình thành và phát triển của Tiếng Anh.
                                    </h4>
                                    <p>Lịch sử hình thành Tiếng Anh bắt đầu từ những chuyến du cư của 3 bộ tộc người Đức,
                                        họ là những người đã xâm chiếm nước Anh trong thế kỷ thứ 5 sau CN.
                                        Ba bộ tộc (đó là Angles, Saxons và Jutes, hiện nay nơi đó ở Đan Mạch và phía bắc Đức) đã băng qua Biển Bắc.
                                        Lúc đó những người bản địa ở Anh đang nói tiếng Xen-Tơ.
                                        Hầu hết họ bị những kẻ xâm lược dồn về phía Tây và Bắc - chủ yếu ở nơi bây giờ là xứ Wale, Scotland và Ireland.
                                        Bọn Angles đến từ Englaland và ngôn ngữ của họ được gọi là Englisc—đó là nguồn gốc của từ England và English.
                                        Về quá trình phát triển của tiếng Anh có khá nhiều những giai đoạn đã ảnh hưởng ít nhiều đến việc hình thành lên chữ viết cũng như cách phát âm của nó.
                                        Chúng tôi xin điểm qua những giai đoạn chính, có ảnh hưởng lớn nhât đến ngôn ngữ mà chúng ta đang tìm hiểu này.
                                    </p>
                                </li>
                            </div>
                        </div>
                        <div class="dotted"></div>
                        <div class="row">
                            <div class="col-7">
                                <li>
                                    <h4 class="color-black">Tiếng Anh cổ (450-1100 sau CN).
                                    </h4>
                                    <p>Những kẻ xâm lược nói ngôn ngữ gần giống nhau, những ngôn ngữ này đã được người Anh phát triển, mà chúng ta gọi là Tiếng Anh cổ (Old English ).
                                        Tiếng Anh cổ được phát âm và viết không giống như Tiếng Anh hiện nay. Ngay cả những người Anh bản địa bây giờ cũng khó mà hiểu được Tiếng Anh cổ.
                                        Tuy thế mà khoảng hơn một nửa những từ Tiếng Anh thông dụng ngày nay lại bắt nguồn từ Tiếng Anh cổ.
                                        Những từ mà chúng ta gặp thường xuyên như be, strong hay water đều được phát hiện là có nguồn gốc trong Tiềng Anh cổ.
                                        Tiếng Anh cổ được sử dụng cho đến khoảng năm 1100.
                                    </p>
                                </li>
                            </div>
                            <div class="col-5">
                                <img class=" img-center" src="img/dog.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                            </div>
                        </div>
                        <div class="dotted"></div>
                        <div class="row">
                            <div class="col-5">
                                <img class=" img-center" src="img/student.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                            </div>
                            <div class="col-7">
                                <li>
                                    <h4 class="color-black">Tiếng Anh trung đại (1100-1500).
                                    </h4>
                                    <p>Vào năm 1066, công tước William của Normandy (một phần của nước Pháp hiện nay), đã xâm chiếm lại Anh từ tay những bộ tộc.
                                        Những kẻ xâm lược mới này đã đem đến một thứ ngôn ngữ mới - tiếng Pháp, và nó đã trở thành ngôn ngữ trong Hoàng gia, của các tầng lớp buôn bán.
                                        Sau một thời gian có sự phần chia tầng lớp ngôn ngữ, tầng lớp thấp hèn thì dùng Tiếng Anh, lớp quý tộc thì lại sử dụng tiếng Pháp. Trong thế kỷ 14, Tiếng Anh mới được trở lại là ngôn ngữ chính thức trên đất nước này, nhưng vẫn có nhiều từ Tiếng Pháp trong đó.
                                        Lúc này là Tiếng Anh trung đại (Middle English).
                                        Nó đã được dùng để viết lên bức thư nổi tiếng Chaucer (c1340-1400), tuy vậy nhưng vẫn còn khác nhiều so với Tiếng Anh hiện nay.
                                    </p>
                                </li>
                            </div>
                        </div>
                        <div class="dotted"></div>
                        <div class="row">
                            <div class="col-7">
                                <li>
                                    <h4 class="color-black">Tiếng Anh cận đại (1500-1800).
                                    </h4>
                                    <p>Càng gần cuối của thời Tiếng Anh trung đại, sự thay đổi bất ngờ và rõ ràng trong cách phát âm (the Great Vowel Shift) đã xảy ra , phát âm những nguyên âm ngày càng ngắn lại.
                                        Từ thế kỷ 16, người Anh đã quan hệ, tiếp xúc với nhiều dân tộc trên khắp thế giới.
                                        Chính điều này, và thời kỳ Phục hưng xảy ra, đã tạo nên nhiều từ mới và nhóm từ mới gia nhập vào ngôn ngữ này.
                                        Sự phát minh máy in cũng cho thấy rằng Tiếng Anh trở nên phổ biến trên các tài liệu in.
                                        Giá sách trở nên rẻ hơn và mọi người có thể học tập qua sách vở.
                                        Ngành in ấn cũng góp phần tạo ra một ngôn ngữ Tiếng Anh tiêu chuẩn, hoàn hảo hơn.
                                        Chính tả và ngữ pháp trở nên hoàn chỉnh, và hình thái ngôn ngữ ở Luân Đôn, là nơi mà nhiều nhà xuất bản ở đó, trở thành thông dụng.
                                        Trong năm 1604 cuốn từ điển Tiếng Anh đầu tiên đã được xuất bản.
                                    </p>
                                </li>
                            </div>
                            <div class="col-5">
                                <img class=" img-center" src="img/medieval.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                            </div>
                        </div>
                        <div class="dotted"></div>
                        <div class="row">
                            <div class="col-5">
                                <img class=" img-center" src="img/people.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                            </div>
                            <div class="col-7">
                                <li>
                                    <h4 class="color-black">Tiếng Anh hiện đại (1800-đến nay).
                                    </h4>
                                    <p>Điểm khác nhau chủ yếu giữa Tiếng Anh cận đại và hiện đại là từ vựng.
                                        Tiếng Anh hiện đại có thêm nhiều từ mới hơn, nó xuất hiện từ 2 nguyên nhân chủ yếu: đầu tiên, Cách mạng Công Nghiệp and Công Nghệ hình thành nên cần nhiều từ mới,
                                        thứ hai, Đế quốc Anh có nhiều thuộc đia (1/4 bề mặt trái đất) hồi bấy giờ, và Tiếng Anh đã phải nhận thêm vào nhiều từ mới qua các nước thuộc địa.
                                    </p>
                                </li>
                            </div>
                        </div>
                        <div class="dotted"></div>
                        <div class="row">
                            <div class="col-7">
                                <li>
                                    <h4 class="color-black">Tính đa dạng của Tiếng Anh.
                                    </h4>
                                    <p>Từ những năm 1600, sự xâm chiếm phía Bắc Mỹ lấy làm thuộc địa của Anh đã dẫn đến sự hình thành nên một Tiếng Anh Mỹ rất đặc trưng.
                                        Một số cách phát âm và cách viết đã bị "hạn định" khi chúng tới Mỹ.
                                        Trong nhiều trường hợp, American English lại giống Tiếng Anh của Shakespeare hơn là Tiếng Anh hiện đại bây giờ.
                                        Một số cách diễn đạt mà người Anh gọi là "Châu Mỹ hóa" đã tồn tại trong thuộc địa của Anh (ví dụ thay trash cho rubbish, loan xem như là động đừ thay thế cho lend, và thay fall cho autumn; còn một số ví dụ khác nữa, frame-up, đã được nhập vào nước Anh lần nữa thông qua các bô phim găng-tơ của Hollywood).
                                        Người Tây Ban Nha cũng tạo ảnh hưởng đến tiếng Anh-Mỹ (dần sau đó đến tiếng Anh-Anh), với những từ như canyon, ranch, stampede và vigilante là một số ví dụ của tiếng Tây Ban Nha đã xâm nhập vào trong Tiếng Anh khi họ định cư ở miền Tây nước Mỹ.
                                        Những từ tiếng Pháp (do Louisiana) và những từ ở Tây Phi (do việc mua bán nô lệ) cũng ảnh hưởng đến tiếng Anh-Mỹ (vì thế, nó "lan" vào tiếng Anh-Anh luôn).
                                        Ngày nay, American English có thế lực đặc biệt, nhờ những ưu thế của Mỹ trong phim ảnh, truyền hình, ca nhạc, thương mại, khoa học kỹ thuật (bao gồm cả Internet).
                                        Nhưng vẫn có những Tiếng Anh thông dụng khác trên thế giới như Australian English, New Zealand English, Canadian English, South African English, Indian English and Caribbean English
                                    </p>
                                </li>
                            </div>
                            <div class="col-5">
                                <img class=" img-center" src="img/now.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                            </div>
                        </div>
                    </ol>
                </div>
            </section>
        </main>
    </div>
    <div class="dotted"></div>
    <div class="navbar navbar-expand-sm img-center2">
        <div class="container nav english">
            <ul class="navbar-nav chu-mau-do right">
                <a id="color-title" class="btn btn-primary right" href="chooseone.php">Back (Quay về)</a>
            </ul>
        </div>
    </div>
</body>
<script src="src/main.js"></script>

</html>