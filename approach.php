<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Approach</title>
    <link rel="shortcut icon" href="img/england.svg" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script type="text/javascript" src="bootstrap.min.css"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Link css -->
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <!-- Latest compiled JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="src/three.r95.min.js"></script>
    <script src="src/vanta.waves.min.js"></script>
    <!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
    <div class="header" id="myHeader">
        <nav class="blue navbar navbar-expand-sm">
            <div class=" container nav english">
                <a href="index.php">
                <h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
                </a>
                <ul class="navbar-nav chu-mau-do right ">
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content" href="chooseone.php" class="btn ">Back</a>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <main>
            <div>
                <section class="margin-section">
                    <div class=" aboutus">
                        <h1>
                            Giới thiệu
                        </h1>
                    </div>
                    <div class="row center-row">
                        <div class="col-4 center font-center-about-us">
                            <a class="center-about-us" href="info.php">Về đồ án</a>
                        </div>
                        <div class="col-4 center font-center-about-us">
                            <a class="center-about-us" href="approach.php">Phương pháp</a>

                        </div>
                        <div class="col-4 center font-center-about-us">
                            <a class="center-about-us" href="group.php">Nhóm</a>
                        </div>
                    </div>
                    <div class="dotted"></div>
                    <div class="row">
                        <div class="col-8">
                            <section class="margin-section">
                                <h2>
                                    Phương pháp của chúng tôi
                                </h2>
                                <span>Chúng tôi tin rằng tất cả mọi người đều có thể học ngôn ngữ một cách dễ dàng. Những bài học ngắn, miễn phí nhưng đầy đủ thông tin cần thiết
                                </span>
                            </section>
                        </div>
                        <div class="col-3"><img class="img-center" src="img/raid.png" alt="Card image" style="width:100%"></div>
                    </div>
                    <div class="dotted"></div>
                    <div>
                        <div class="row">
                            <div class="col-3"><img class="img-center" src="img/dev.png" alt="Card image" style="width:70%"></div>
                            <div class="col-8">
                                <section class="margin-section">
                                    <h2>
                                        Với các thứ tự được sắp xếp tối giản nhất có thể
                                    </h2>
                                    <span>Giúp cho người học có thể tự khám phá ra những nguyên tắc của riêng mình mà không cần tập trung tới quy tắc của ngôn ngữ - tương tự như khi một đứa trẻ mới học nói tiếng mẹ đẻ.
                                        Phương pháp này được gọi là "học tự nhiên", rất thích hợp để phát triển một nền tảng ngôn ngữ vững chắc cũng như các quy tắc của ngôn ngữ đó.
                                    </span>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="dotted"></div>
                    <div>
                        <div class="row">
                            <div class="col-8">
                                <section class="margin-section">
                                    <h2>
                                        Liên tục phát triển
                                    </h2>
                                    <span>Chúng tôi luôn thấy ý kiến/đánh giá của mỗi người và từ đó phát triển tranh web theo hướng tự nhiên nhất, basic, không cầu kì, cũng như giúp bạn cảm thấy thoãi mái nhất
                                        khi học trong trang web của chúng tôi.
                                    </span>
                                </section>
                            </div>
                            <div class="col-3"><img class="img-center" src="img/eng.jpg" alt="Card image" style="width:100%">
                            </div>
                        </div>
                    </div>
                    <div class="dotted"></div>
                </section>
            </div>
        </main>
    </div>
    <div class="dotted"></div>
    <div class="footer backgroud">
        <div class="header-footer "></div>
    </div>
</body>
<script src="src/main.js"></script>

</html>