<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book</title>
    <link rel="shortcut icon" href="img/england.svg" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script type="text/javascript" src="bootstrap.min.css"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Link css -->
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <!-- Latest compiled JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="src/three.r95.min.js"></script>
    <script src="src/vanta.waves.min.js"></script>
    <!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
    <div class="header" id="myHeader">
        <nav class="blue navbar navbar-expand-sm">
            <div class="container nav english">
                <a href="index.php">
                <h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
                </a>
                <ul class="navbar-nav chu-mau-do right ">
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content">Hi <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></a>
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content" href="logout.php" class="btn ">Sign Out</a>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container-itwfbd-0 padding-book">
        <div class="GroupProducts__Wrapper-qw45r9-0 padding-content-book">
            <div class="header-book">
                <span>
                    <img class="logo-icon" src="img/logo.png" alt="Card image">
                </span>
                <div>
                    <h3 class="title-header">Những quyển sách giúp bạn học tốt hơn (trader by Tiki.vn).</h3>
                </div>
            </div>
            <div class="body">
                <div class="row img-center padding-allborder">
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/hack-nao-1500-phien-ban-2020-p48504524.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/ts/product/e8/76/1d/23f7f9741620fb7b4f32a3ef038767e6.png" alt="" style="width: 70%">
                                <h4 class="border-part3">Hack Não 1500</h4>

                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/luyen-noi-tieng-anh-dot-pha-hacking-your-english-speaking-tang-bookmark-doc-dao-p41540280.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/ts/product/93/57/bb/0a645f2ddc673b30e2c60fd535031216.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Luyện Nói Tiếng Anh Đột Phá</h4>

                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/bo-sach-all-in-one-tu-hoc-tieng-anh-cho-hoc-sinh-thpt-va-thcs-bo-2-cuon-p25979779.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/ts/product/41/fb/d7/5000d14ef34d33950fcad1d819ea6465.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Bộ Sách All In One Tự Học Tiếng Anh </h4>

                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/combo-tu-hoc-tieng-anh-sieu-toc-cho-nguoi-moi-bat-dau-tang-sach-tu-hoc-giao-tiep-tieng-anh-qua-truyen-cuoi-p15830361.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/ts/product/71/f6/ed/80721e85e9754fafd2769ef5558326e0.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Combo Tự Học Tiếng Anh Siêu Tốc</h4>

                            </div>
                        </a>
                    </div>
                </div>
                <div class="row img-center padding-allborder">
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/tieng-anh-co-ban-tron-bo-2-tap-p3869871.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/ts/product/5e/29/5c/a9d7167c538a8a4f5672b53fed48c871.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Tiếng Anh Cơ Bản ( Trọn Bộ 2 Tập )</h4>

                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/combo-bo-4-cuon-effortless-english-hoc-tieng-anh-nhu-nguoi-ban-ngu-cung-aj-hoge-tang-kem-bookmark-pl-p36369601.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/ts/product/62/da/a9/d1b6d8c500e06d8e35a2eaaec720a3f2.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Combo Bộ 4 Cuốn Effortless English</h4>

                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/combo-sach-tri-mat-goc-tieng-anh-brain-booster-nghe-phan-xa-tieng-anh-bang-cong-nghe-song-nao-danh-cho-nguoi-mat-goc-mindmap-english-grammar-ngu-phap-tieng-anh-bang-so-do-tu-duy-phat-am-tieng-anh-hoan-hao-tang-so-tay-tu-vung-p31691132.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/ts/product/5b/c3/64/a65f99c9d0f41be80cbdf0f5f547f9a9.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Combo sách Trị mất gốc tiếng Anh</h4>

                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/grammar-for-you-basic-bi-quyet-chinh-phuc-ngu-phap-tieng-anh-co-ban-p689381.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/media/catalog/product/n/g/ngu-phap-co-ban-1.u5168.d20170615.t180155.978958.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Grammar For You (Basic)</h4>

                            </div>
                        </a>
                    </div>
                </div>
                <div class="row img-center padding-allborder">
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/combo-bo-sach-danh-van-tieng-anh-hay-cho-be-tron-4-tap-tang-kem-bookmark-books-p18139073.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/ts/product/aa/71/db/d70c232744b4f2c905feffe7def0a2b9.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Combo bộ sách Đánh Vần Tiếng Anh hay cho bé</h4>

                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/bo-sach-ngu-phap-tieng-anh-ban-chay-so-1-tai-han-quoc-grammar-gateway-basic-grammar-gateway-intermediate-tang-tickbook-dac-biet-p36153866.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/ts/product/59/ef/91/5357e5619def43b95b4a2a37f4d0f6a4.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Bộ Sách Ngữ Pháp Tiếng Anh</h4>

                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/tieng-anh-kinh-doanh-kem-cd-sach-bo-tui-p506680.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/media/catalog/product/i/m/img436.u335.d20160530.t161547.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Tiếng Anh Kinh Doanh (Kèm CD) - Sách Bỏ Túi </h4>

                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-book" role="button" tabindex="0" href="https://tiki.vn/ren-ky-nang-lam-bai-trong-am-ngu-am-mon-tieng-anh-phien-ban-2019-co-mai-phuong-tang-kem-bookmark-p22399943.html?src=search&amp;2hi=0&amp;keyword=s%C3%A1ch%20ti%E1%BA%BFng%20anh&amp;_lc=Vk4wMzkwMTcwMDg%3D">
                            <div class="border-part">
                                <div class="border-part1"></div>
                                <img class="product-image img-responsive" src="https://salt.tikicdn.com/cache/280x280/ts/product/df/ab/29/7806d8681223718037fec3adafd6e6ac.jpg" alt="" style="width: 70%">
                                <h4 class="border-part3">Rèn Kỹ Năng Làm Bài Trọng Âm Ngữ</h4>

                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-expand-sm img-center2">
        <div class="container nav english">
            <ul class="navbar-nav chu-mau-do right">
                <a id="color-title" class="btn btn-primary right" href="chooseone.php">Back (Quay về)</a>
            </ul>
        </div>
    </div>
    <div class="footer backgroud">
        <div class="header-footer "></div>
    </div>
</body>
<script src="src/main.js"></script>

</html>