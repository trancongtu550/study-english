<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Group</title>
    <link rel="shortcut icon" href="img/england.svg" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script type="text/javascript" src="bootstrap.min.css"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Link css -->
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <!-- Latest compiled JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="src/three.r95.min.js"></script>
    <script src="src/vanta.waves.min.js"></script>
    <!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
    <div class="header" id="myHeader">
        <nav class="blue navbar navbar-expand-sm">
            <div class=" container nav english">
                <a href="index.php">
                <h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
                </a>
                <ul class="navbar-nav chu-mau-do right ">
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content" href="chooseone.php" class="btn ">Back</a>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <main>
            <section class="margin-section">
                <div class="center-section aboutus">
                    <h1>
                        Giới thiệu
                    </h1>
                </div>
                <div class="row center-row">
                    <div class="col-4 center font-center-about-us">
                        <a class="center-about-us" href="info.php">Về đồ án</a>
                    </div>
                    <div class="col-4 center font-center-about-us">
                        <a class="center-about-us" href="approach.php">Phương pháp</a>

                    </div>
                    <div class="col-4 center font-center-about-us">
                        <a class="center-about-us" href="group.php">Nhóm</a>
                    </div>
                </div>
                <div class="dotted"></div>
                <section class="margin-section">
                    <div class="center-section">
                        <div class="row">
                            <div class="col-sm-4">
                                <img class="img-banner" src="img/why.jpg" alt="Card image" style="width:80%">
                            </div>
                            <div class="col-sm-8">
                                <h1>
                                    Thông tin website
                                </h1>
                                <span>
                                    Đây là website giúp bạn phát triển khả năng Tiếng Anh của mình, để có thể nâng cao trình độ học vấn
                                </span>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="dotted"></div>
                <section class="margin-section">
                    <div class="center-section">
                        <div class="row">
                            <div class="col-sm-8">
                                <h1>
                                    Giao tiếp với người các nước khác nhau.
                                </h1>
                                <span>
                                    Các cách giao tiếp ứng xử, có thể học được dễ dàng nếu như bạn biết được Tiếng Anh, bạn có thể đi đó đây mà không phải ngại mình là người nước khác.<br>
                                    Tự tin giao tiếp với người bản xứ để học thêm nhiều điều mới lạ.<br>
                                    Thay đổi lối sống hằng ngày, có thể nói chuyện với AI, đặt lịch, hay những thứ được setup bằng Tiếng Anh.

                                </span>
                            </div>
                            <div class="col-sm-4">
                                <img class="img-banner" src="img/hand.jpg" alt="Card image" style="width:80%">
                            </div>
                        </div>
                    </div>
                </section>
                <div class="dotted"></div>
                <section class="margin-section">
                    <div class="center-section">
                        <div class="row">
                            <div class="col-sm-4">
                                <img class="img-banner" src="img/brain.gif" alt="Card image" style="width:80%">
                            </div>
                            <div class="col-sm-8">
                                <h1>
                                    Phương pháp học Tiếng Anh
                                </h1>
                                <span>
                                    1. Vượt qua tâm lý sợ hãi và kiên trì đến cùng. <br>
                                    2. Chuẩn bị vốn ngôn ngữ “từ vựng” vững chắc. <br>
                                    3. Dừng việc học ngữ pháp theo cách truyền thống. <br>
                                    4. Phát âm chuẩn và lưu loát là 2 câu chuyện hoàn toàn khác nhau. <br>
                                    5. Tự tạo môi trường giao tiếp tiếng Anh và đắm chìm trong đó.
                                </span>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </main>
    </div>
    <div class="dotted"></div>
    <div class="footer backgroud">
        <div class="header-footer "></div>
    </div>
</body>
<script src="src/main.js"></script>

</html>