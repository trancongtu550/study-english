<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Welcome</title>
	<link rel="shortcut icon" href="img/england.svg" />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="bootstrap.min.css"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Link css -->
	<link rel="stylesheet" type="text/css" href="css/theme.css">
	<!-- Latest compiled JavaScript -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
	<script src="src/three.r95.min.js"></script>
	<script src="src/vanta.waves.min.js"></script>
	<!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
	<nav id="myHeader" class="blue navbar navbar-expand-sm">
		<div class="container nav english">
			<a href="index.php">
			<h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
			</a>
			<ul class="navbar-nav chu-mau-do right ">
				<a id="color-title" class="nav-item nav-link chu-mau-do head-content">Hi <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></a>
				<a id="color-title" class="nav-item nav-link chu-mau-do head-content" href="logout.php" class="btn ">Sign Out</a>
			</ul>
		</div>
	</nav>
	<div class="items-center wc-flex">
		<div class="row">
			<div class="img-center">
				<h1 class="display-4">Welcome !</h1>
				<p class="lead">Hãy chọn ngôn ngữ bạn muốn học</p>
				<hr class="my-4">
				<a class="border-english max-width1" role="button" tabindex="0" href="chooseone.php">
					<div class="border-part">
						<img class="" src="img/england.svg" alt="Card image" style="width:50%; border-radius: 16px; background: white">
						<h1 class="border-part3">English</h1>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="footer backgroud">
		<div class="header-footer "></div>
	</div>
</body>
<script src="src/main.js"></script>

</html>