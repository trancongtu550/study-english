<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
	header("location: login.php");
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Video Lesson</title>
    <link rel="shortcut icon" href="img/england.svg" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script type="text/javascript" src="bootstrap.min.css"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Link css -->
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <!-- Latest compiled JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <script src="src/three.r95.min.js"></script>
    <script src="src/vanta.waves.min.js"></script>
    <!-- A grey horizontal navbar that becomes vertical on small screens -->

</head>

<body class="body" id="body">
    <div class="header" id="myHeader">
        <nav class="blue navbar navbar-expand-sm">
            <div class=" container nav english">
                <a href="index.php">
                <h1 id="color-title" class="my-0 mr-md-auto font-weight-normal"> WCUL<span class="badge badge-primary new">Beta</span></h1>
                </a>
                <ul class="navbar-nav chu-mau-do right ">
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content">Hi <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></a>
                    <a id="color-title" class="nav-item nav-link chu-mau-do head-content" href="logout.php" class="btn ">Sign Out</a>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <main>
            <div class="img-center">
                <h1 class="display-4">Welcome to Videolessons!</h1>
                <p class="lead">Choose one step (hãy họn 1 bước)</p>
                <hr class="my-4">
            </div>
            <div>
                <div class="row img-center padding-allborder">
                    <div class="col-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson1.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/healthy-food.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">The Super Bowl's Big Day</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson2.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/amazon.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">The Life of the Amazon</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson3.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/salmon.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">Raising Salmon</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson4.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/phone.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">Mobile Novels</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row img-center padding-allborder">
                    <div class="col-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson5.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/idol.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">True American Idol</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson6.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/housing.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">Subprime Housing Crisis</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson7.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/note.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">Post It Notes</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson8.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/world.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">The World Series</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row img-center padding-allborder">
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson9.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/china.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">Yao Ming</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson10.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/television.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">TV Depression</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson11.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/guitar.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">Electric Guitars</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson12.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/heartbreak.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">Broken Heart</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row img-center padding-allborder">
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson13.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/mtv.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">MTV</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson14.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/fitness.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">Oval Office Fitness Plan</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson15.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/bad-boy.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">Bad Boys</h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson16.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/ao-dai.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">Ao Dai</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row img-center padding-allborder">
                    <div class="col-sm-3">
                        <a class="border-english" role="button" tabindex="0" href="Videolesson/lesson17.php">
                            <div class="border-part">
                                <img class="" src="img/videolessimg/fast-food.svg" alt="Card image" style="width:50%; border-radius: 16px; ">
                                <h4 class="border-part3">English - French</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <div class="dotted"></div>
    <div class="navbar navbar-expand-sm img-center2">
        <div class="container nav english">
            <ul class="navbar-nav chu-mau-do right">
                <a id="color-title" class="btn btn-primary right" href="chooseone.php">Back (Quay về)</a>
            </ul>
        </div>
    </div>
</body>
<script src="src/main.js"></script>

</html>